//
//  GraphView.hpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 09/01/2019.
//  Copyright © 2019 Marek Kulik. All rights reserved.
//

#ifndef GraphView_hpp
#define GraphView_hpp

#include <vector>             // add vectors
#include <bitset>             // add bitsets

#include "InstructionData.hpp"

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/adjacency_list.hpp"
#include <boost/graph/graphviz.hpp>

using std::vector;
using std::string;
using namespace boost;

class GraphView {
private:
    struct GraphNode {
        GraphNode(int id_, uint32_t start_, uint32_t end_) : id{id_}, start{start_}, end{end_} {}
        
        int id;         // we dont need it, next version will remove it, start might be our ID
        intptr_t start;
        intptr_t end;
        int jmp_offset;
        bool cond; // only valid when jmp_offset is not 0
        bool dealt_with; // ready to go;
        
        vector<string> lines;
        vector<int> connect_to;
    };
    
public:
    GraphView();
    ~GraphView();
    GraphNode countInstructions(int16_t nodeID);
    void process(string txt, Channel mnemonic, int offset, bool _2way);
    void process2ndStage();
    void process3rdStage();
    vector<GraphNode> getNodes();
    vector<GraphNode> nodes;
    
    string makeDot();
private:
 
    
    GraphView::GraphNode* getLastNode();
    GraphNode splitNode(GraphNode node, uint32_t split_addr);
};




#endif /* GraphView_hpp */
