//
//  GraphView.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 09/01/2019.
//  Copyright © 2019 Marek Kulik. All rights reserved.
//

#include "GraphView.hpp"

#include <iostream>

GraphView::GraphView() {
    // Create first node
    auto node = new GraphNode(0, 0x0, 0x0);
    node->dealt_with = false;
    node->jmp_offset = 0x0;
    
    this->nodes.push_back(*node);
}

void GraphView::process(string txt, Channel mnemonic, int offset, bool _2way) {
    auto node = getLastNode();

    //add stuff to node
    node->end += 4; //  ̶t̶h̶i̶s̶ ̶m̶i̶g̶h̶t̶ ̶b̶e̶ ̶w̶r̶o̶n̶g̶ ̶.̶.̶.̶.̶.̶.̶ ̶!̶!̶!̶!̶!̶ It's correct !
    node->lines.push_back(txt);
    
    if (mnemonic == +Channel::B && offset != 0x0) {
        node->jmp_offset = offset;
        node->cond = _2way;

        auto new_node = new GraphNode(node->id + 1, (node->end), (node->end));
        new_node->dealt_with = false;
        this->nodes.push_back(*new_node);
    }
}

void GraphView::process2ndStage() {
    bool all_done = false;      // mark if we are done with alle nodes

    // Lambda, Lambda
    // To break out from nasted loops
    [&] {
        for(auto &&node : this->nodes) {      // access by forwarding reference
            
            // check if node is upper half of splitted node && not processed yet
            if (node.dealt_with) {
                //std::cout << "SKIP node id : " << node.id << std::endl;
                continue;
            }
            
            if (node.jmp_offset == 0) {
                //std::cout << "SKIP 2 node id : " << node.id << std::endl;
                continue;
            }

            
            // start addr of new node
            auto addr_to_search = node.jmp_offset + node.end - 4; // -4 might be error when we addding in Dissasembler +4
            
            //std::cout << "TRying to deal with node nr. == " << node.id << ", start = " << node.start << ", end = " << node.end << ", offset  = " << node.jmp_offset << ", >>> SEARCH : " << node.jmp_offset + node.end - 4 << std::endl;

            for(auto&& searched_node : this->nodes) {
                // Check if we found node
                if ((addr_to_search - searched_node.start) < (searched_node.end-searched_node.start)) {
                    #ifdef DEBUG
                        std::cout << "^^^^^^ LOOKING FOR SPLIT ^^^^^  [ I found you !!! ]  (For NODE  : " << node.id << ") :: [found node ID] >>>> " << searched_node.id << " NODe(start) -->" << searched_node.start << " :: NODe(end) -->" << searched_node.end << std::endl;
                    #endif
                    
                    // check if we need to split node
                    if (addr_to_search == searched_node.start) {
                        // NO ?? G00d :)
                    } else {
                        //std::cout << "MAKING SPLIT Searched ADDRES : " << addr_to_search << " :: NODe(start) -->" << searched_node.start << " :: NODe(end) -->" << searched_node.end << std::endl;
                        
                        // split node()
                        // I dont know how it works when we assign new nodes to vectors during loop
                        // For sure We CANT insert them in the middle, we might loop through new ones annd ommit old ones.
                        // always push back


                        // BUT when we split node before is "dealt with" in first loop
                        // then we have a problem..
                        // so what we do ? Just re run algo with marking "dealt with" nodes
                        // super not effective but it's fine
                        // Question is HOW to break of a nested loop --> goto or lambda



                        /// WITH SPLIT WE NEED TO connect FRONT with LOWER.... ALWAYS

                        // Split node will return pointer? to new node to assign..

                        auto new_split_node = this->splitNode(searched_node, addr_to_search);
                        
                        searched_node.connect_to.push_back(new_split_node.id);

                        // mark as done
                        node.dealt_with = true;

                        // rerun
                        return;

                    }
                    
                    // mark as done
                    node.dealt_with = true;
                    
                    break;
                }



            } // end of 2nd loop
        } // end of 1st loop

        all_done = true;

    }(); // end of lambda

    if (!all_done) {
       // std::cout << std::endl;
     //   std::cout << std::endl;
     //   std::cout << std::endl;
         this->process2ndStage();
    }
    

}

// connect nodes wit(c)h BRANCH
void GraphView::process3rdStage() {
    for(auto &&node : this->nodes) {
        if (node.jmp_offset == 0)
            continue;
        
        auto addr_to_search = node.jmp_offset + node.end - 4;
        
        for(auto&& searched_node : this->nodes) {
            if ((addr_to_search - searched_node.start) < (searched_node.end-searched_node.start)) {
                if (addr_to_search != searched_node.start)
                    continue;
                
                if (node.cond) {
                    auto addr_to_search2 = node.end;
                    
                    auto id_f = std::find_if(this->nodes.begin(), this->nodes.end(), [&](const GraphView::GraphNode & o) {
                        return o.start == addr_to_search2;
                    });
                    
                    if (id_f == this->nodes.end()) {
                        std::cout << "FAILT TO FUND" << std::endl;
                    } else {
                        auto next_one = this->nodes.at(id_f - this->nodes.begin());
                        node.connect_to.push_back(next_one.id);
                    }

                }
                    
                node.connect_to.push_back(searched_node.id);
                
                break;
            }
        }
    }
}


GraphView::GraphNode GraphView::splitNode(GraphView::GraphNode node, uint32_t split_addr) {
    // let's keep the loop, nodes are not sorted so it doesn't matter
    // binary_search wont make a difference...
    for(auto&& searched_node : this->nodes) {
        if (searched_node.id != node.id)
            continue;
        
        // Lower half of the node aka NEW node
        auto new_node = new GraphNode(this->nodes.back().id + 1, split_addr, node.end);
        new_node->jmp_offset = node.jmp_offset;
        new_node->cond = node.cond;
        new_node->dealt_with = node.dealt_with;
        
        #ifdef DEBUG
            std::cout << ":::SPLITTiNG::: Split OF N0de Id --->  " << searched_node.id << " ##### new NODe ID ---> " << new_node->id << std::endl;
        #endif
        
        // Separate, assign, remove !!
        int how_many_lines = (split_addr - searched_node.start) / 4;
        vector<string> new_down_strig((node.lines.begin() + how_many_lines), node.lines.end());
        new_node->lines = new_down_strig;
        
        #ifdef DEBUG
            std::cout << ":::SPLITTiNG::: removing lines from HEAD node ID : " << searched_node.id << " *****  new [HEAD] lines count: " << (how_many_lines + 1)  << std::endl;
        #endif
        
        searched_node.lines.erase(searched_node.lines.begin() + how_many_lines, searched_node.lines.end());

        
        if (!searched_node.connect_to.empty()) {
            new_node->connect_to = searched_node.connect_to;
            searched_node.connect_to.clear();
        }
        
        searched_node.end = split_addr ;
        searched_node.jmp_offset = 0x0;
        searched_node.cond = false;
        
        this->nodes.push_back(*new_node);
        
        return *new_node;
    }
    
    // dummy (let's check for -1)
    return *new GraphNode(-1, 0, 0);

}

string GraphView::makeDot() {
    struct Vertex { std::string foo; }; // Head
    struct Edge { std::string blah; }; // Line
    
    typedef adjacency_list< listS, vecS, directedS > digraph;
    
    using graph_t  = adjacency_list<listS, vecS, directedS, Vertex, Edge >;
    using vertex_t = graph_traits<graph_t>::vertex_descriptor;
    using edge_t   = graph_traits<graph_t>::edge_descriptor;
    
    // instantiate a digraph object with 0 vertices
    graph_t g(0);
    
    
    // Add Vertexsss $$ edges
    for (auto const &node3 : this->getNodes()) {
        std::string s;
        for (const auto &piece : node3.lines) {
            s += piece;
            s += "\n";
        }
       
        add_vertex(Vertex{s}, g);
    }
    
    // Add edges
    for (auto const &node3 : this->getNodes()) {
        for (auto const &node4 : node3.connect_to) {
            add_edge(node3.id, node4, g);
        }
    }
    
    std::stringstream output;
    write_graphviz(output, g, [&] (auto& out, auto v) {
        out << "[label=\"" << g[v].foo << "\"]";
    },
                   [&] (auto& out, auto e) {
                       out << "[label=\"" << g[e].blah << "\"]";
                   });
    
    return output.str();
}

GraphView::GraphNode* GraphView::getLastNode() {
    return &(this->nodes.at(this->nodes.size() - 1));
}

vector<GraphView::GraphNode> GraphView::getNodes() {
    return this->nodes;
}


GraphView::~GraphView() {
    // we need to clean this mess (Sprint 4 [last one]  ͡° ͜ʖ ͡°))
}
