//
//  InstructionData.hpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 24/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#ifndef InstructionData_hpp
#define InstructionData_hpp

// How great is c++  ಠ_ಠ
#include "Extras/ENUM_WITH_TYPE.hpp"

#include <bitset>               // add bitsets
#include <tuple>                // add tuples
#include <functional>           // add functional


// This is the worst part. We are limited to 64? type of instructions if we dont want to keep another array to convert enum to string.... no solution for this even in C++17 :D
BETTER_ENUM(Channel, uint32_t,  B = 1, BLX, BXJ, BX, BL,
                                MUL, MLA, UMAAL, MLS, UMULL, UMLAL, SMULL, SMLAL,
                                AND, EOR, SUB, RSB, ADD, SBC, ADC, RSC, MVN, BIC, ROR, RRX, ASR, LSR, LSL, MOV, ORR, CMN, CMP, TEQ, TST,
                                STR, STRT, LDRT, LDR, STRBT, STRB, LDRBT, LDRB, MOVW, MOVT, NOP, DBG,
            INVALID)

typedef std::bitset<32> opcode;
typedef std::tuple<std::string, int32_t, bool> returnData;
typedef std::tuple<Channel, std::function<returnData(u_long, Channel)>> ResultDecode;

struct InstructionData {
    std::string mask;
    
    Channel mnemonic;
    std::function<returnData(u_long, Channel)> callback;
};

#endif /* InstructionData_hpp */
