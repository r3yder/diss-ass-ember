//
//  main.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 23/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include <iostream>
#include "Decoder.hpp"


#include <bitset>

#include <tuple>                // add tuples
#include <functional>           // add functional


#include "Extras/hex_decoder/hex.hpp"




#include "GraphView/GraphView.hpp"


int main(int argc, const char * argv[]) {
    static const int bufferSize = 1024;
    
    
    uint8_t* src = new uint8_t[bufferSize];
    uint8_t* dest = new uint8_t[bufferSize] {0};
    //std::cout << "Wprowadź HEX ARM Code: ";
    std::cin >> src;
    
    
    // This is fast...
    // SIMD (vectorized) hex string encoder/decoder
    // Requires x86/64 AVX2 (Haswell or later).
    // by: https://github.com/zbjornson/fast-hex
    decodeHexVec(dest, src, bufferSize);
    
    auto graph = new GraphView();
    
    // When you don't know how to do it in a modern way .....
    for (uint32_t *ptr = (uint32_t*)(dest) ; (ptr) <= (uint32_t*)((dest) + bufferSize) ; ptr++ ) {
        //auto final_value = ntohl(*ptr);
        auto final_value = (uint32_t)(*ptr);
        
        if (final_value == 0)
            break;
        
        auto instruction = std::bitset<32>(final_value);
        auto test = Decoder::decode(instruction);
        
        auto function_address = std::get<1>(test);
        auto mnemonic = std::get<0>(test);
        
        if (mnemonic != +Channel::INVALID) {
            auto [str_output, offset, _2way] = function_address(instruction.to_ulong(), mnemonic);
            
            graph->process(str_output, mnemonic, offset, _2way);
            
        }
    }
    
    #ifdef DEBUG
        for(auto const &node : graph->getNodes()) {
            std::cout << " RESULT MAKING GRAPH STAGE NR <<< 1 >>" << std::endl;
            std::cout << "NODe ID: " << node.id << " :: [start] " << node.start << " :: [end] " << node.end << " ++++++ [offset jmp]" << node.jmp_offset << " :: COND[2way]: " << node.cond << "STRING [last] : " << node.lines.back() << std::endl;
        }
    #endif

    graph->process2ndStage();
    
    #ifdef DEBUG
    for (auto const &node : graph->getNodes()) {
        std::cout << " RESULT MAKINg GRAPH STAGE NR <<< 2 >> aka Splitting N0DES" << std::endl;
        std::cout << "NODE ID: " << node.id << " :: [start]  " << node.start << " ::  [end] " << node.end << " ++++++ ";
        for (auto const &connect : node.connect_to)
            std::cout << connect << " ";
        std::cout << std::endl;

    }
    #endif
    
    graph->process3rdStage();
    
    #ifdef DEBUG
    for (auto const &node : graph->getNodes()) {
        std::cout << " RESULT MAKINg GRAPH STAGE NR <<< 3 >> aka Connecting N0DES" << std::endl;
        std::cout << "NODE ID: " << node.id << " :: [start]  " << node.start << " ::  [end] " << node.end << " ++++++ ";
        for (auto const &connect : node.connect_to)
            std::cout << connect << " ";

        std::cout << std::endl;
    }
    #endif
    

    string graphDot = graph->makeDot();
    
    std::cout << graphDot << std::endl;
    
    return 0;
   
}
