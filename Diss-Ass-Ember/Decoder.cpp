//
//  Decoder.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 23/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Decoder.hpp"

ResultDecode Decoder::decode(opcode opcode) {
    for (auto const& instruction : Decoder::instructionArray) {
        // I don't think its gonna be faster using String
        // but it's more clear ?
        
        // we should despace it once with one buffer (all instructions) and then just pointer + 8 with this char* buffer
        // and cast it to (u_int32) but speed it's not purpose of this project..
        // vector transformation are here just for fun and (bitset) too (String also)
        
        auto pattern = instruction.mask;    // This is String
        
        char *array = &pattern[0];
        sse4_despace_branchless(array, 39);  // That's the size with spaces...
        
        // null sign
        pattern.resize(32);
        
        // make mask
        auto pattern2 = pattern;
        
        // MAke a mask from pattern
        std::replace( pattern2.begin(), pattern2.end(), 'x', '0');
        
        // keep to check final result
        std::bitset<32> compare(pattern2);
        
        std::transform(std::begin(pattern), std::end(pattern), std::begin(pattern), [](auto ch) {
            switch (ch) {
                case 'x':
                    return '0';
                case '0':
                    return '1';
                case '1':
                    return '1';
            }
            return ch;
        });
        
        std::bitset<32> mask(pattern);
        std::bitset<32> result(mask&opcode);
        
        // XOR to check
        if (std::bitset<32>(compare^result).none()) {
            return ResultDecode(instruction.mnemonic, instruction.callback);
        }
        
    }
    
    return ResultDecode(Channel::INVALID, nullptr);
}
