//
//  Decoder.hpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 23/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#ifndef Decoder_hpp
#define Decoder_hpp

#include "InstructionData.hpp"
#include "Disassembler/Disassembler.hpp"

// Only for removeing spaces (why not / its fun project) so its here
#include "Extras/despacer/despacer.h"

class Decoder {
public:
    
    static inline InstructionData const instructionArray[] = {
        
        // Special ones
        {"xxxx 0011 0010 0000 1111 0000 0000 0000", Channel::NOP, Disassembler::Special_NOP_DBG},
        {"xxxx 0011 0010 0000 1111 0000 1111 0000", Channel::DBG, Disassembler::Special_NOP_DBG},
        
        
        // DATA
        {"xxxx 0000 000x xxxx xxxx xxxx xxx0 xxxx", Channel::AND, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 001x xxxx xxxx xxxx xxx0 xxxx", Channel::EOR, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 010x xxxx xxxx xxxx xxx0 xxxx", Channel::SUB, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 011x xxxx xxxx xxxx xxx0 xxxx", Channel::RSB, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 100x xxxx xxxx xxxx xxx0 xxxx", Channel::ADD, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 101x xxxx xxxx xxxx xxx0 xxxx", Channel::ADC, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 110x xxxx xxxx xxxx xxx0 xxxx", Channel::SBC, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0000 111x xxxx xxxx xxxx xxx0 xxxx", Channel::RSC, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        

        {"xxxx 0001 0001 xxxx 0000 xxxx xxx0 xxxx", Channel::TST, Disassembler::DataReg_TST_CMN_TEQ_CMP},
        {"xxxx 0001 0011 xxxx 0000 xxxx xxx0 xxxx", Channel::TEQ, Disassembler::DataReg_TST_CMN_TEQ_CMP},
        {"xxxx 0001 0101 xxxx 0000 xxxx xxx0 xxxx", Channel::CMP, Disassembler::DataReg_TST_CMN_TEQ_CMP},
        {"xxxx 0001 0111 xxxx 0000 xxxx xxx0 xxxx", Channel::CMN, Disassembler::DataReg_TST_CMN_TEQ_CMP},
        {"xxxx 0001 100x xxxx xxxx xxxx xxx0 xxxx", Channel::ORR, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        
        {"xxxx 0001 101x 0000 xxxx 0000 0000 xxxx", Channel::MOV, Disassembler::DataReg_MOV},
        
        {"xxxx 0001 101x 0000 xxxx xxxx x000 xxxx", Channel::LSL, Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR},
        {"xxxx 0001 101x 0000 xxxx xxxx x010 xxxx", Channel::LSR, Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR},
        {"xxxx 0001 101x 0000 xxxx xxxx x100 xxxx", Channel::ASR, Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR},
        {"xxxx 0001 101x 0000 xxxx 0000 0110 xxxx", Channel::RRX, Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR},
        {"xxxx 0001 101x 0000 xxxx xxxx x110 xxxx", Channel::ROR, Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR},
        
        {"xxxx 0001 110x xxxx xxxx xxxx xxx0 xxxx", Channel::BIC, Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx xxxx 1110 000x 0000 xxxx xxx0 xxxx", Channel::MVN, Disassembler::Branch_B},
        
        
        // DATA 22222
        {"xxxx 0010 000x xxxx xxxx xxxx xxxx xxxx", Channel::AND, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 001x xxxx xxxx xxxx xxxx xxxx", Channel::EOR, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 010x xxxx xxxx xxxx xxxx xxxx", Channel::SUB, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 011x xxxx xxxx xxxx xxxx xxxx", Channel::RSB, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 100x xxxx xxxx xxxx xxxx xxxx", Channel::ADD, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 101x xxxx xxxx xxxx xxxx xxxx", Channel::ADC, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 110x xxxx xxxx xxxx xxxx xxxx", Channel::SBC, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0010 111x xxxx xxxx xxxx xxxx xxxx", Channel::RSC, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0011 0001 xxxx 0000 xxxx xxxx xxxx", Channel::TST, Disassembler::DataImm_TST_TEQ_CMP_CMN},
        {"xxxx 0011 0011 xxxx 0000 xxxx xxxx xxxx", Channel::TEQ, Disassembler::DataImm_TST_TEQ_CMP_CMN},
        {"xxxx 0011 0101 xxxx 0000 xxxx xxxx xxxx", Channel::CMP, Disassembler::DataImm_TST_TEQ_CMP_CMN},
        {"xxxx 0011 1000 xxxx 0000 xxxx xxxx xxxx", Channel::CMN, Disassembler::DataImm_TST_TEQ_CMP_CMN},
        {"xxxx 0011 100x xxxx xxxx xxxx xxxx xxxx", Channel::ORR, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0011 101x 0000 xxxx xxxx xxxx xxxx", Channel::MOV, Disassembler::DataImm_MOV_MVN},
        {"xxxx 0011 110x xxxx xxxx xxxx xxxx xxxx", Channel::BIC, Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC},
        {"xxxx 0011 111x 0000 xxxx xxxx xxxx xxxx", Channel::MVN, Disassembler::DataImm_MOV_MVN},
        {"xxxx 0011 0000 xxxx xxxx xxxx xxxx xxxx", Channel::MOVW, Disassembler::DataImm_MOVW_MOVT},
        {"xxxx 0011 0100 xxxx xxxx xxxx xxxx xxxx", Channel::MOVT, Disassembler::DataImm_MOVW_MOVT},
        
        
        
        
        /// LAODS STORES
        {"xxxx 0100 x010 xxxx xxxx xxxx xxxx xxxx", Channel::STRT, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0110 x010 xxxx xxxx xxxx xxx0 xxxx", Channel::STRT, Disassembler::StoreReg_STRT_STRBT_STR},
        
        {"xxxx 0100 0000 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0100 1000 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 0000 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 0010 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 1000 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 0101 xxxx xxxx xxxx xxxx xxxx", Channel::STR, Disassembler::StoreImm_STRT_STRBT_STR},
        
        {"xxxx 0110 0000 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0110 1000 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 0000 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 0010 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 1000 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 1010 xxxx xxxx xxxx xxx0 xxxx", Channel::STR, Disassembler::StoreReg_STRT_STRBT_STR},

        {"xxxx 0100 x011 xxxx xxxx xxxx xxxx xxxx", Channel::LDRT, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0110 x011 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRT, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        
        {"xxxx 0100 0001 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0100 1001 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 0001 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 0011 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 1001 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 1011 xxxx xxxx xxxx xxxx xxxx", Channel::LDR, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        
        {"xxxx 0110 0001 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0110 1001 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 0001 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 0011 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 1001 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 1011 xxxx xxxx xxxx xxx0 xxxx", Channel::LDR, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        
        
        {"xxxx 0100 x110 xxxx xxxx xxxx xxxx xxxx", Channel::STRBT, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0110 x110 xxxx xxxx xxxx xxx0 xxxx", Channel::STRBT, Disassembler::StoreReg_STRT_STRBT_STR},
        
        {"xxxx 0100 0100 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0100 1100 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 0100 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 0110 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 1100 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        {"xxxx 0101 1110 xxxx xxxx xxxx xxxx xxxx", Channel::STRB, Disassembler::StoreImm_STRT_STRBT_STR},
        
        {"xxxx 0110 0100 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0110 1100 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 0100 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 0110 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 1100 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        {"xxxx 0111 1110 xxxx xxxx xxxx xxx0 xxxx", Channel::STRB, Disassembler::StoreReg_STRT_STRBT_STR},
        
        
        
        {"xxxx 0100 x111 xxxx xxxx xxxx xxxx xxxx", Channel::LDRBT, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0110 x111 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRBT, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        
        {"xxxx 0100 0101 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0100 1101 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 0101 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 0111 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 1101 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        {"xxxx 0101 1111 xxxx xxxx xxxx xxxx xxxx", Channel::LDRB, Disassembler::LoadImm_LDRT_LDRBT_LDR},
        
        {"xxxx 0110 0101 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0110 1101 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 0101 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 0111 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 1101 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        {"xxxx 0111 1111 xxxx xxxx xxxx xxx0 xxxx", Channel::LDRB, Disassembler::LoadReg_LDRT_LDRBT_LDR},
        
        
        
        // Branches
        {"xxxx 1010 xxxx xxxx xxxx xxxx xxxx xxxx", Channel::B, Disassembler::Branch_B},
        {"xxxx 0001 0010 1111 1111 1111 0010 xxxx", Channel::BXJ, Disassembler::Branch_BLX_BX_BXJ},
        {"xxxx 0001 0010 1111 1111 1111 0001 xxxx", Channel::BX, Disassembler::Branch_BLX_BX_BXJ},
        {"xxxx 0001 0010 1111 1111 1111 0011 xxxx", Channel::BLX, Disassembler::Branch_BLX_BX_BXJ},
        // That order ?
        {"1111 101x xxxx xxxx xxxx xxxx xxxx xxxx", Channel::BLX, Disassembler::Branch_BLX_IMM},
        {"xxxx 1011 xxxx xxxx xxxx xxxx xxxx xxxx", Channel::BL, Disassembler::Branch_BL_IMM},
        
        
        
        // MULTIPLY
        {"xxxx 0000 000x xxxx 0000 xxxx 1001 xxxx", Channel::MUL, Disassembler::Multiply_MUL},
        {"xxxx 0000 0110 xxxx xxxx xxxx 1001 xxxx", Channel::MLS, Disassembler::Multiply_MLS},
        {"xxxx 0000 001x xxxx xxxx xxxx 1001 xxxx", Channel::MLA, Disassembler::Multiply_MLA},
        {"xxxx 0000 0100 xxxx xxxx xxxx 1001 xxxx", Channel::UMAAL, Disassembler::Multiply_UMAAL},
        
        
        {"xxxx 0000 1000 xxxx xxxx xxxx 1001 xxxx", Channel::UMULL, Disassembler::Multiply_MLAL_MULL},
        {"xxxx 0000 1010 xxxx xxxx xxxx 1001 xxxx", Channel::UMLAL, Disassembler::Multiply_MLAL_MULL},
        {"xxxx 0000 1100 xxxx xxxx xxxx 1001 xxxx", Channel::SMULL, Disassembler::Multiply_MLAL_MULL},
        {"xxxx 0000 1110 xxxx xxxx xxxx 1001 xxxx", Channel::SMLAL, Disassembler::Multiply_MLAL_MULL}
    };
    
    static ResultDecode decode(opcode opcode);

};


#endif /* Decoder_hpp */
