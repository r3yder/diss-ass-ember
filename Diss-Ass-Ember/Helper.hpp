//
//  Helper.hpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 23/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#ifndef Helper_hpp
#define Helper_hpp

#include <iostream>


#define imm ror32(op & 0xFF, (op >> 7) & 0x1E)


#define regs(x) Helper::registerNames[x]
#define conds Helper::conditionCodeNames

#define Rd  regs((opcode >> 12) & 0xF)
#define Rn  regs((opcode >> 16) & 0xF)
#define Rs  regs((opcode >> 8) & 0xF)
#define Rm  regs(opcode & 0xF)

#define S_  (((opcode >> 20) & 0x1) ? "S" : "")

#define Cond_ Helper::conditionCodeNames[(opcode >> 28) & 0xF]


class Helper {
public:
    static inline std::string const conditionCodeNames[] = { "EQ", "NE", "CS", "CC", "MI", "PL", "VS", "VC", "HI", "LS", "GE", "LT", "GT", "LE", "", "" };
    static inline std::string const registerNames[] = { "r0", "r1",  "r2",  "r3",  "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "sp", "lr", "pc" };
    
    static inline u_long rotl32 (u_long n, unsigned int c) {
        const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);  // assumes width is a power of 2.
        
        // assert ( (c<=mask) &&"rotate by type width or more");
        c &= mask;
        return (n<<c) | (n>>( (-c)&mask ));
    }
    
    static inline u_long rotr32 (u_long n, unsigned int c) {
        const unsigned int mask = (CHAR_BIT*sizeof(n) - 1);
        
        // assert ( (c<=mask) &&"rotate by type width or more");
        c &= mask;
        return (n>>c) | (n<<( (-c)&mask ));
    }

};

#endif /* Helper_hpp */
