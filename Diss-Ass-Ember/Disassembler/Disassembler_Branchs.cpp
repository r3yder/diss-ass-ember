//
//  Disassembler_Branchs.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 30/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"

Disassembler::returnData Disassembler::Branch_B(u_long opcode, Channel mnemonic) {
    //// To działa
    ///// Kondycje są ok. Nie mozemy miec array w macro
    //// Ujmene nie sa sprawdzone ale powinny dzialac
    
    
    auto sign = (opcode&0x00800000) == 0 ? "+" : "-";
    auto immaa =  ((opcode&0xFFFFFF) << 2);
    
    immaa += 0x8;
    
    if (strncmp(sign, "-", 1) == 0) {
        immaa = (immaa^0x3FFFFFF);
        immaa += 0x1;
    }
    
  //  immaa += 0x8;
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << sign << "0x" << std::hex << immaa ;

    bool _2_way = Cond_ == "" ? false : true;

    
    return Disassembler::returnData(output.str(), (opcode&0x00800000) == 0 ? immaa : (immaa*-1), _2_way);
}

Disassembler::returnData Disassembler::Branch_BLX_BX_BXJ(u_long opcode, Channel mnemonic) {
    //// To działa
    std::stringstream output;
    output << mnemonic._to_string() <<  Cond_ << " " << Rm;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::Branch_BL_IMM(u_long opcode, Channel mnemonic) {
    ///// OK so lets check negative values, how it works
    ///// EBFFFFBA -->  BLT    #0xFFFFFFB4
    ///// switch edians === BAFFFFEB
    ///// we dont care about cond so BA is out
    ///// so we have 00FFFFEB ---> 0000 0000 1111 1111 1111 1111 1110 1011
    ///// we shit left                   0000 0011 1111 1111 1111 1111 1010 1111  == 3FFFFAF
    ///// 26b is 1 so we XOR 26 bits     0000 0000 0000 0000 0000 0000 0101 0000 == 0x50 = 80
    
    auto sign = (opcode&0x00800000) == 0 ? "+" : "-";
    auto immaa =  ((opcode&0xFFFFFF) << 2);
    
    if (strncmp(sign, "-", 1) == 0)
        immaa = (immaa^0x3FFFFFF);
    
    immaa += 0x8;
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << sign << "0x" << std::hex << immaa;
    
    bool _2_way = Cond_ == "" ? false : true;
    
    return Disassembler::returnData(output.str(), (opcode&0x00800000) == 0 ? immaa : (immaa*-1), _2_way);
}


Disassembler::returnData Disassembler::Branch_BLX_IMM(u_long opcode, Channel mnemonic) {
    
    auto H = (opcode >> 24) & 1;
    auto IMM = (opcode&((1 << 24) - 1));
    
    if ((IMM >> 23) & 1)
        IMM = (IMM | 0xff000000) << 2;
    else
        IMM = (IMM << 2);
    
    IMM |= H << 1;
    

    auto sign = (opcode&0x00800000) == 0 ? "+" : "-";
    
    std::stringstream output;
    output << mnemonic._to_string() << " " << sign << "0x" << std::hex << IMM;
    
    return Disassembler::returnData(output.str(), (opcode&0x00800000) == 0 ? IMM : (IMM*-1), false);
}

