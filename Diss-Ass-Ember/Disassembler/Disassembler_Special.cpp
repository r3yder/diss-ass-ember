//
//  dsfsdf.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 03/01/2019.
//  Copyright © 2019 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"


Disassembler::returnData Disassembler::Special_NOP_DBG(u_long opcode, Channel mnemonic) {
    return Disassembler::returnData(mnemonic._to_string(), 0, false);
}
