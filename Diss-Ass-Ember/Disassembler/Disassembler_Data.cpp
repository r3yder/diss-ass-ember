//
//  ddddd.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 09/12/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"

Disassembler::returnData Disassembler::DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC(u_long opcode, Channel mnemonic) {
    auto va = (opcode&0xFF);
    auto ror = (opcode&0xF00);
    
    auto imma = Helper::rotr32(va, ror);
    
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << Rn << ", " << "#" << imma;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::DataImm_TST_TEQ_CMP_CMN(u_long opcode, Channel mnemonic) {
    auto va = (opcode&0xFF);
    auto ror = (opcode&0xF00);
    
    auto imma = Helper::rotr32(va, ror);
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rn << ", " << "#" << imma;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::DataImm_MOV_MVN(u_long opcode, Channel mnemonic) {
    auto va = (opcode&0xFF);
    auto ror = (opcode&0xF00);
    
    auto imma = Helper::rotr32(va, ror);
    
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << "#" << imma;
    
    return Disassembler::returnData(output.str(), 0, false);
}


Disassembler::returnData Disassembler::DataImm_MOVW_MOVT(u_long opcode, Channel mnemonic) {
    auto va = (opcode&0xFF);
    auto ror = (opcode&0xF00);
    
    
    auto imma = ((opcode&0xF0000)<< 12) | (opcode&0xFFF);
        
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << "#" << imma << std::endl;
    
    return Disassembler::returnData(output.str(), 0, false);
}


//void Disassembler::Branch_B(opcode opcode, Channel mnemonic) {
//    auto bits = getBits(opcode);
//    auto test = Cond_(bits);
//    
//    auto rn = (bits&0x000F0000);
//    auto rd = (bits&0x0000F000);
//    
//    auto s = (bits&0x00100000>>20) ? "S" : "";
//    
//    auto rotation = (bits&0x00000F00) >> 8;
//    auto rotation_value = (bits&0x000000FF);
//    //// SHIFT RIGHT ??
//    auto rotation_value_FINAAL = rotation_value >> (rotation*2);
//    
//    
//    
//    std::cout << mnemonic._to_string() << s << Helper::Cond_itionCodeNames[test] << " "
//    << Helper::registerNames[rd] << ", " << Helper::registerNames[rn] << ", #" << rotation_value_FINAAL << std::endl;
//    
//}
//
//
//void Disassembler::Branch_B(opcode opcode, Channel mnemonic) {
//    auto bits = getBits(opcode);
//    auto test = Cond_(bits);
//    
//    auto rn = (bits&0x000F0000);
//    auto rotation = (bits&0x00000F00) >> 8;
//    auto rotation_value = (bits&0x000000FF);
//    //// SHIFT RIGHT ??
//    auto rotation_value_FINAAL = rotation_value >> (rotation*2);
//    
//    
//    std::cout << mnemonic._to_string() << Helper::Cond_itionCodeNames[test] << " "
//    << Helper::registerNames[rn] << ", #" << rotation_value_FINAAL << std::endl;
//    
//}
//
//
//void Disassembler::Branch_B(opcode opcode, Channel mnemonic) {
//    auto bits = getBits(opcode);
//    auto test = Cond_(bits);
//    
//    auto rd = (bits&0x0000F000);
//    auto imm_ = (bits&0x000F0000) >> 16 << 12 | (bits&0x00000FFF);
//    
//
//    
//    std::cout << mnemonic._to_string() << Helper::Cond_itionCodeNames[test] << " "
//    << Helper::registerNames[rd] << ", #" << imm_ << std::endl;
//    
//}


Disassembler::returnData Disassembler::DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC(u_long opcode, Channel mnemonic) {
    
    /// IMM
    auto imm_value = (opcode&0x00000F80) >> 7;
    auto imm_type = (opcode&0x00000060) >> 5;
    
    auto shift_name = "";
    if (imm_type == 0)
        shift_name = "LSL";
    else if (imm_type == 1)
        shift_name = "LSR";
    else if (imm_type == 2)
        shift_name = "ASR";
    else if (imm_type == 3)
        shift_name = "ROR";
    
    if (imm_value == 0 && imm_type == 3)
        shift_name = "RRX";
    else if (imm_value == 0 && (imm_type == 1 || imm_type == 2))
        imm_value = 32;
    else if (imm_type == 0 && imm_type == 0)
        shift_name = "";
    
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << Rn << ", " << Rm << ", " << shift_name << "#" << imm_value;
    
    return Disassembler::returnData(output.str(), 0, false);
}


Disassembler::returnData Disassembler::DataReg_TST_CMN_TEQ_CMP(u_long opcode, Channel mnemonic) {
    
    /// IMM
    auto imm_value = (opcode&0x00000F80) >> 7;
    auto imm_type = (opcode&0x00000060) >> 5;
    
    auto shift_name = "";
    if (imm_type == 0)
        shift_name = "LSL";
    else if (imm_type == 1)
        shift_name = "LSR";
    else if (imm_type == 2)
        shift_name = "ASR";
    else if (imm_type == 3)
        shift_name = "ROR";
    
    if (imm_value == 0 && imm_type == 3)
        shift_name = "RRX";
    else if (imm_value == 0 && (imm_type == 1 || imm_type == 2))
        imm_value = 32;
    else if (imm_type == 0 && imm_type == 0)
        shift_name = "";
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rn << ", " << Rm << ", " << shift_name << "#" << imm_value;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::DataReg_MOV(u_long opcode, Channel mnemonic) {
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << Rm;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::DataImm_LSL_LSR_ASR_RRX_ROR(u_long opcode, Channel mnemonic) {
    std::stringstream output;
    
    if (mnemonic == +Channel::RRX)
        output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << Rm;
    else {
        auto amount = (opcode&0x00000F80) >> 7;
        
        if (mnemonic == +Channel::ASR && amount == 0)
            amount = 32;
        
        output << mnemonic._to_string() << S_ << Cond_ << " " << Rd << ", " << Rm << ", #" << amount;
    }
    
    return Disassembler::returnData(output.str(), 0, false);
}

