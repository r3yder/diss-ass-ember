//
//  Disassembler.hpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 24/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#ifndef Disassembler_hpp
#define Disassembler_hpp
#include <iostream>

#include "../InstructionData.hpp"
#include "../Helper.hpp"

//#include <bitset>



#include <sstream>

class Disassembler {
public:
    // Used to return output
    typedef std::tuple<std::string, int32_t, bool> returnData;
    
    // HELPERS
    static std::string LSR(u_long opcode);
    static std::string LSI(u_long opcode);
    
    // Special (one way instruction, LIke nop, dbg)
    static returnData Special_NOP_DBG(u_long opcode, Channel mnemonic);
    
    // Branch
    static returnData Branch_B(u_long opcode, Channel mnemonic);
    static returnData Branch_BLX_BX_BXJ(u_long opcode, Channel mnemonic);
    static returnData Branch_BL_IMM(u_long opcode, Channel mnemonic);
    static returnData Branch_BLX_IMM(u_long opcode, Channel mnemonic);
    
    //MULTIPLY
    static returnData Multiply_UMAAL(u_long opcode, Channel mnemonic);
    static returnData Multiply_MLAL_MULL(u_long opcode, Channel mnemonic);
    static returnData Multiply_MUL(u_long opcode, Channel mnemonic);
    static returnData Multiply_MLS(u_long opcode, Channel mnemonic);
    static returnData Multiply_MLA(u_long opcode, Channel mnemonic);

    //DATA
    static returnData DataReg_MOV(u_long opcode, Channel mnemonic);
    static returnData DataReg_TST_CMN_TEQ_CMP(u_long opcode, Channel mnemonic);
    static returnData DataImm_LSL_LSR_ASR_RRX_ROR(u_long opcode, Channel mnemonic);
    static returnData DataReg_ADD_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC(u_long opcode, Channel mnemonic);
    
    static returnData DataImm_AND_EOR_SUB_RSB_ADD_ADC_SBC_RSC_ORR_BIC(u_long opcode, Channel mnemonic);
    static returnData DataImm_TST_TEQ_CMP_CMN(u_long opcode, Channel mnemonic);
    static returnData DataImm_MOV_MVN(u_long opcode, Channel mnemonic);
    static returnData DataImm_MOVW_MOVT(u_long opcode, Channel mnemonic);
    
    // Stores
    static returnData StoreReg_STRT_STRBT_STR(u_long opcode, Channel mnemonic);
    static returnData StoreImm_STRT_STRBT_STR(u_long opcode, Channel mnemonic);
    
    // Loads
    static returnData LoadReg_LDRT_LDRBT_LDR(u_long opcode, Channel mnemonic);
    static returnData LoadImm_LDRT_LDRBT_LDR(u_long opcode, Channel mnemonic);
    
private:
    static u_long getBits(opcode opcode);
    
};

#endif /* Disassembler_hpp */
