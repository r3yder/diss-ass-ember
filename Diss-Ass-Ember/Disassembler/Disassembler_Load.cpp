//
//  dddd.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 25/12/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"


Disassembler::returnData Disassembler::LoadReg_LDRT_LDRBT_LDR(u_long opcode, Channel mnemonic) {
    // RT = Rd
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rd << ", " << LSR(opcode)  ;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::LoadImm_LDRT_LDRBT_LDR(u_long opcode, Channel mnemonic) {
    // RT = Rd
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rd << ", " << LSI(opcode)  ;
    
    return Disassembler::returnData(output.str(), 0, false);
}
