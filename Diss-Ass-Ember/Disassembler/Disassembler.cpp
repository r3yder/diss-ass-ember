//
//  Disassembler.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 24/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"


// It's need to extends our opcode (TO DO)
u_long Disassembler::getBits(opcode opcode) {
    auto code = opcode.to_ulong();

    return code;
}

std::string Disassembler::LSI(u_long opcode) {
    bool indexed = (opcode&0x1000000);
    bool add = (opcode&0x800000);
    bool writeback = (opcode&0x200000);
    
    std::string sign = add ? "" : "-";
    
    bool isExtraLoad = (opcode & 0x0C000000) == 0;
    
    u_long immediate = 0x00000000;
    
    if (isExtraLoad)
        immediate = ((opcode&0xF00) << 4) | (opcode&0xF);
    else
        immediate = (opcode&0xFFF);
    
    
    if (indexed) {
        if (writeback)
            return ("[" + Rn + ", #" + sign + std::to_string(immediate) + "]!");
        
        std::string offsetImmediateStr = immediate == 0 ? "" : (", #" + sign + std::to_string(immediate));
        return "[" + Rn + offsetImmediateStr + "]";
    }
    
    // Post-indexed variant.
    std::string postIndexedImmediateStr = immediate == 0 ? "" : ", #" + sign + std::to_string(immediate) ;
     return "[" + Rn + "]" + postIndexedImmediateStr;
    
}

std::string Disassembler::LSR(u_long opcode) {
    bool indexed = (opcode&0x1000000);
    bool add = (opcode&0x800000);
    bool writeback = (opcode&0x200000);
    
    
    auto sign = add ? "" : "-";
    
    bool isExtraStore = (opcode & 0x0C000000) == 0;
    
    // Extra load stores cannot perform shifts.
    std::string shift = "";
    if (!isExtraStore) {
        
        /// IMM
        auto imm_value = (opcode&0x00000F80) >> 7;
        auto imm_type = (opcode&0x00000060) >> 5;
        
        std::string shift_name = "";
        if (imm_type == 0)
        shift_name = "LSL";
        else if (imm_type == 1)
        shift_name = "LSR";
        else if (imm_type == 2)
        shift_name = "ASR";
        else if (imm_type == 3)
        shift_name = "ROR";
        
        if (imm_value == 0 && imm_type == 3)
        shift_name = "RRX";
        else if (imm_value == 0 && (imm_type == 1 || imm_type == 2))
        imm_value = 32;
        else if (imm_type == 0 && imm_type == 0)
        shift_name = "";
        
        shift = shift_name + "#";
        shift += imm_value;
    }
    
    if (indexed) {
        auto writebackBang = writeback ? "!" : "";
        std::string temp = "[" + Rn + ", " + sign + Rm + shift + "]" + writebackBang;
        return temp;
    }
    
    std::string temp = "[" + Rn + "], " + sign + Rm + shift;
    return temp;
}
