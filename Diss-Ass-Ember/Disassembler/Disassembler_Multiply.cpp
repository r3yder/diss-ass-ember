//
//  Disassembler_Multiply.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 24/11/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"


Disassembler::returnData Disassembler::Multiply_UMAAL(u_long opcode, Channel mnemonic) {
    auto rdlo = regs((opcode >> 12) & 0xF);
    auto rdhi = regs((opcode >> 16) & 0xF);
    
    auto rn = regs((opcode >> 8) & 0xF);
    auto rm = regs((opcode >> 0) & 0xF);
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << rdlo << ", " << rdhi << ", " << rn << ", " << rm;
    
    return Disassembler::returnData(output.str(), 0, false);
}


Disassembler::returnData Disassembler::Multiply_MLAL_MULL(u_long opcode, Channel mnemonic) {
    
    auto rdlo = regs((opcode >> 12) & 0xF);
    auto rdhi = regs((opcode >> 16) & 0xF);
    
    auto rn = regs((opcode >> 8) & 0xF);
    auto rm = regs((opcode >> 0) & 0xF);
    
    auto s_or_u = (opcode&0x00400000>>22) ? "S" : "U";
    
    std::stringstream output;
    output << s_or_u << mnemonic._to_string() << S_ << Cond_ << " " << rdlo << ", " << rdhi << ", " << rn << ", " << rm;

    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::Multiply_MUL(u_long opcode, Channel mnemonic) {
    // Names are wrong but bits are correct.
    
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rn << ", " << Rs << ", " << Rm;

    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::Multiply_MLS(u_long opcode, Channel mnemonic) {
    // Names are wrong but bits are correct.
    
    auto RA = regs((opcode >> 12) & 0xF);
    
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rn << ", " << Rs << ", " << Rm << ", " << RA;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::Multiply_MLA(u_long opcode, Channel mnemonic) {
    // Names are wrong but bits are correct.
    
    auto RA = regs((opcode >> 12) & 0xF);
    
    std::stringstream output;
    output << mnemonic._to_string() << S_ << Cond_ << " " << Rn << ", " << Rs << ", " << Rm << ", " << RA;
    
    return Disassembler::returnData(output.str(), 0, false);
}
