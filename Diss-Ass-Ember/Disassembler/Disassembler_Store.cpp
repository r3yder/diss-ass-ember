//
//  Disasss.cpp
//  Diss-Ass-Ember
//
//  Created by Marek Kulik on 25/12/2018.
//  Copyright © 2018 Marek Kulik. All rights reserved.
//

#include "Disassembler.hpp"



Disassembler::returnData Disassembler::StoreReg_STRT_STRBT_STR(u_long opcode, Channel mnemonic) {
    // RT = Rd
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rd << ", " << LSR(opcode) ;
    
    return Disassembler::returnData(output.str(), 0, false);
}

Disassembler::returnData Disassembler::StoreImm_STRT_STRBT_STR(u_long opcode, Channel mnemonic) {
    // RT = Rd
    std::stringstream output;
    output << mnemonic._to_string() << Cond_ << " " << Rd << ", " << LSI(opcode) ;
    
    return Disassembler::returnData(output.str(), 0, false);
}
